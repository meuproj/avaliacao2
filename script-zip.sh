#!/bin/bash

MENU=('------------------------------Menu------------------------------
	Digite "sair" para sair do programa
	Digite "criar" para criar um novo arquivo .zip
       	Digite "modificar" para modificar um arquivo .zip existente
	')

SUBMENU=('------------------------------Menu------------------------------
	1-Listar o conteúdo do arquivo .zip
	2-Pré-visualizar algum arquivo que está compactado neste zip
	3-Adicionar arquivos ao zip
	4-Remover Arquivos do zip
	5-Extrair todo o conteúdo do zip
	6-Extrair arquivos específicos do zip
	')

while true 
do	
	echo "$MENU"	
	read -p "Digite uma opção: " opcao
	if [ $opcao = "sair" ]
	then
		echo "Saindo do programa..."
		break
	elif [ $opcao = "criar" ]
	then
		read -p "Criar arquivo .zip de nome: " zipar
		read -p "Digite o nome dos arquivos para zipar separados por um espaço: " arq
		zip $zipar $arq
		VALOR=$zipar.zip

	elif [ $opcao = "modificar" ]
	then
		read -p "Digite o nome do arquivo para modificar: " mod
		VALOR=$mod
	else
		echo "Opção inválida"
	fi
	CONT=s
	while [ $CONT = s ]
	do	
		echo "$SUBMENU"
		read -p "Escolha o que fazer: " subop
		if [ $subop = "1" ]
		then
			unzip -l $VALOR
		elif [ $subop = "2" ]
		then
			vim $VALOR
		elif [ $subop = "3" ]
		then
			read -p "Qual arquivo quer adicionar?: " adc
			zip $VALOR $adc
		elif [ $subop = "4" ]
		then
			read -p "Qual arquivo quer remover?: " remover
			zip -d $VALOR $remover
		elif [ $subop = "5" ]
		then
			unzip $VALOR		
		elif [ $subop = "6" ]
			read -p "Digite o nome do(s) arquivo(s) que deseja extrair separados por um espaço: " ext
		then
			unzip $VALOR $ext
		fi
		read -p "Continuar com o arquivo $VALOR?[s/n]: " resp
		if [ $resp = "s" ]
		then
			CONT=s
		else	
			CONT=n
		fi
		
	done
done

